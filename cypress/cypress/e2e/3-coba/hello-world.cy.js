/// <reference types="cypress" />

describe('Basic desktop test', () => {
    beforeEach(() => {
        cy.viewport(1920,1080)
        cy.visit('https://codedamn.com')

        cy.then(() => {
            window.localStorage.setItem('__auth__token', token)
        })
    })
    
    it('Webpage loads', () => {
        cy.visit('https://codedamn.com')

        // cy.contains('Learn Programming').should('exist')
        // cy.get('div#root').should('exist')
        // cy.get('div#noroot').should('not.exist')
        // cy.get('.asyncComponent > div > a')
        // const el = cy.get('[data-testid=menutoogle]')
        // expect(el.text() === 'hello')
    })

    it('Login page looks good', ()=> {
        // cy.viewport(1920,1080)
        // cy.visit('https://codedamn.com')

        cy.contains('Sign in').click()
        cy.contains('Sign in to codedamn').should('exist')
        cy.get('[data-testid="google-oauth-btn"]').should('exist')
        cy.contains('Sign in with Google').should('exist')
        cy.contains('Sign in with GitHub').should('exist')

    })

    it('Login should display error', ()=> {
        // cy.viewport(1920,1080)
        // cy.visit('https://codedamn.com')

        cy.contains('Sign in').click()
        
        cy.get('[data-testid="username"]').type('admin')
        cy.get('[data-testid="password"]').type('admin')
        
        cy.get('[data-testid="login"]').click()

        cy.contains('Unable to authorize').should('exist')
    })
    // it.only('Every basic element exist on phone', () => {
    //     cy.viewport('iphone-x')
    //     cy.visit('https://codedamn.com')
    // })
})